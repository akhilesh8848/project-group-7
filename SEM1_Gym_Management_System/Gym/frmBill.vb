﻿Imports System.Drawing.Printing
Public Class frmBill
    Dim sr As Integer
    Dim da As New OleDb.OleDbDataAdapter
    Dim ds As New DataSet
    Dim ds1 As New DataSet
    Dim ds3 As New DataSet

    Private BMP As Bitmap

    Private Sub subA(ByVal s, ByVal args)
        args.Graphics.DrawImage(BMP, 0, 0)
        args.HasMorePages = False
    End Sub
    Private Sub frmBill_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblbdate.Text = Format(Today, "dd/MM/yyyy")
        Module1.scalarquery("select max(bid)+1 from bill")
        lblbno.Text = nb
        sr = 1


        Dim dr As DataRow
        da = New OleDb.OleDbDataAdapter("select cname from gymmember", con)
        ds.Clear()
        da.Fill(ds)
        ComboBox1.Items.Clear()

        Dim x As Integer
        While x < ds.Tables(0).Rows.Count
            dr = ds.Tables(0).Rows(x)
            ComboBox1.Items.Add(dr(0))
            x = x + 1
        End While

        da = New OleDb.OleDbDataAdapter("select pname from product ", con)
        ds3.Clear()
        da.Fill(ds3)
        'Dim x As Integer
        ComboBox2.Items.Clear()

        x = 0
        While x < ds3.Tables(0).Rows.Count
            dr = ds3.Tables(0).Rows(x)
            ComboBox2.Items.Add(dr(0))
            x = x + 1
        End While


        ListView1.Items.Clear()
        ListView1.View = View.Details
        ListView1.FullRowSelect = True


        ListView1.Columns.Add("Sr", 50)
        ListView1.Columns.Add("Product Name", 240)
        ListView1.Columns.Add("Quantity", 150)
        ListView1.Columns.Add("Price", 130)
        ListView1.Columns.Add("Total", 130)


    End Sub

    Private Sub ComboBox1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Leave
        lblcname.Text = ComboBox1.Text

        Dim dr As DataRow
        da = New OleDb.OleDbDataAdapter("select cmob from gymmember where cname='" & ComboBox1.Text & "'", con)
        ds1.Clear()
        da.Fill(ds1)
        If ds1.Tables(0).Rows.Count > 0 Then
            dr = ds1.Tables(0).Rows(0)
            lblmobile.Text = dr(0)
        End If


    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Dim ch As Integer

        ch = MsgBox("Confirm Print", vbYesNo + vbQuestion, "Print")
        If ch = vbYes Then
            Module1.inupdel("insert into bill values(" & CInt(lblbno.Text) & ",'" & lblmobile.Text & "','" & lblbdate.Text & "','" & lblsubtotal.Text & "','" & lblgst.Text & "','" & lblgrand.Text & "')")

            Dim pd As New PrintDocument
            Dim pdialog As New PrintDialog
            Dim ppd As New PrintPreviewDialog
            BMP = New Bitmap(GroupBox1.Width, GroupBox1.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            GroupBox1.DrawToBitmap(BMP, New Rectangle(0, 0, GroupBox1.Width, GroupBox1.Height))
            AddHandler pd.PrintPage, AddressOf subA
            '(Sub(s, args) args.Graphics.DrawImage(BMP, 0, 0)args.HasMorePages = False     End Sub)
            'choose a printer
            If pdialog.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
                MsgBox("Print Cancel")
                Exit Sub
            End If
            pd.PrinterSettings.PrinterName = pdialog.PrinterSettings.PrinterName
            If pd.PrinterSettings.CanDuplex.ToString Then
                pd.PrinterSettings.Duplex = Duplex.Vertical
            End If
            'Preview the document
            ppd.Document = pd
            If ppd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                pd.Print()
                ppd.Dispose()

            Else
                '            MsgBox("Print Cancel")
            End If
        Else
            MsgBox("Operation Cancelled")
        End If
        Me.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        If TextBox1.Text <> "" And ComboBox2.Text <> "" Then

            Module1.inupdel("insert into billshort values(" & CInt(lblbno.Text) & ",'" & ComboBox2.Text & "','" & TextBox1.Text & "')")
            Module1.inupdel("update product set pqty=pqty-" & CInt(TextBox1.Text) & " where pname='" & ComboBox2.Text & "'")

        End If


        Dim item As New ListViewItem(sr)
        item.SubItems.Add(ComboBox2.Text)
        item.SubItems.Add(TextBox1.Text)
        Module1.scalarquery("select pprice from product where pname='" & ComboBox2.Text & "'")
        '   MsgBox(nb)

        item.SubItems.Add(nb)
        item.SubItems.Add(CInt(TextBox1.Text) * nb)
        lblsubtotal.Text = CInt(lblsubtotal.Text) + (CInt(TextBox1.Text) * nb)
        lblgst.Text = Math.Round(CInt(lblsubtotal.Text) * 0.05)
        lblgrand.Text = CInt(lblsubtotal.Text) + CInt(lblgst.Text)

        ListView1.Items.Add(item)
        sr = sr + 1


        TextBox1.Clear()
        ComboBox2.Text = ""
        Button1.Enabled = False

    End Sub

    Private Sub TextBox1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.Leave
        Module1.scalarquery("select sum(pqty) from product where pname='" & ComboBox2.Text & "'")
        If nb < CInt(TextBox1.Text) Then
            MsgBox("Available Stock Is " & nb)
            Button1.Enabled = False
        Else
            Button1.Enabled = True

        End If

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class