﻿Imports System.Drawing.Printing
Public Class frmReceipt
    Private BMP As Bitmap

    Private Sub subA(ByVal s, ByVal args)
        args.Graphics.DrawImage(BMP, 0, 0)
        args.HasMorePages = False
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        Dim ch As Integer

        ch = MsgBox("Confirm Print", vbYesNo + vbQuestion, "Print")
        If ch = vbYes Then

            Dim pd As New PrintDocument
            Dim pdialog As New PrintDialog
            Dim ppd As New PrintPreviewDialog
            BMP = New Bitmap(GroupBox1.Width, GroupBox1.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            GroupBox1.DrawToBitmap(BMP, New Rectangle(0, 0, GroupBox1.Width, GroupBox1.Height))
            AddHandler pd.PrintPage, AddressOf subA
            '(Sub(s, args) args.Graphics.DrawImage(BMP, 0, 0)args.HasMorePages = False     End Sub)
            'choose a printer
            If pdialog.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
                MsgBox("Print Cancel")
                Exit Sub
            End If
            pd.PrinterSettings.PrinterName = pdialog.PrinterSettings.PrinterName
            If pd.PrinterSettings.CanDuplex.ToString Then
                pd.PrinterSettings.Duplex = Duplex.Vertical
            End If
            'Preview the document
            ppd.Document = pd
            If ppd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                pd.Print()
                ppd.Dispose()

            Else
                '            MsgBox("Print Cancel")
            End If
        Else
            MsgBox("Operation Cancelled")
        End If
        Me.Close()
        frmmembership.Dispose()


    End Sub
End Class