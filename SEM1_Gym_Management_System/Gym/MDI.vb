﻿Public Class MDI

    Private Sub MDI_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        End


    End Sub

    Private Sub MDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub TrainerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrainerToolStripMenuItem.Click

    End Sub

    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click
        If MsgBox("Do you really Want to Logout", vbYesNo + vbQuestion, " Logout") = vbYes Then
            End
        End If
    End Sub

    Private Sub AddUpdateTrainerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddUpdateTrainerToolStripMenuItem.Click
        frmStaff.Show()

    End Sub

    Private Sub AddNewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddNewToolStripMenuItem.Click
        frmNewMember.ShowDialog()

    End Sub

    Private Sub CreateNewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateNewToolStripMenuItem.Click
        frmPackage.ShowDialog()

    End Sub

    Private Sub SupplierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SupplierToolStripMenuItem.Click
        addsupplier.Show()

    End Sub

    Private Sub AddNewToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddNewToolStripMenuItem1.Click
        frmEquipment.ShowDialog()

    End Sub

    Private Sub UpdateProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateProductToolStripMenuItem.Click
        updateproduct.ShowDialog()

    End Sub

    Private Sub NewProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewProductToolStripMenuItem.Click
        frmAddProduct.ShowDialog()

    End Sub

    Private Sub UpdateStockToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateStockToolStripMenuItem.Click
        updatepstock.ShowDialog()

    End Sub

    Private Sub ViewTrainersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewTrainersToolStripMenuItem.Click
        frmViewEmployee.veflg = 5

        frmViewEmployee.Show()

    End Sub

    Private Sub ViewAllEmployeesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewAllEmployeesToolStripMenuItem.Click
        frmViewEmployee.veflg = 6

        frmViewEmployee.Show()

    End Sub

    Private Sub ViewAllPackagesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewAllPackagesToolStripMenuItem.Click
        frmViewPackage.ShowDialog()

    End Sub

    Private Sub CountEquipmentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CountEquipmentToolStripMenuItem.Click
        frmEquipCount.ShowDialog()

    End Sub

    Private Sub MemberReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MemberReportToolStripMenuItem.Click
        memberreport.ShowDialog()

    End Sub

    Private Sub EmployeesReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmployeesReportToolStripMenuItem.Click
        employeerep.ShowDialog()

    End Sub

    Private Sub ProductReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductReportToolStripMenuItem.Click
        productreport.ShowDialog()

    End Sub

    Private Sub BillToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BillToolStripMenuItem.Click
        frmBill.ShowDialog()

    End Sub

    Private Sub ManageSubscriptionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ManageSubscriptionToolStripMenuItem.Click
        frmmembership.ShowDialog()

    End Sub

    Private Sub ListMembersToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListMembersToolStripMenuItem.Click
        frmMemberships.ShowDialog()

    End Sub


End Class