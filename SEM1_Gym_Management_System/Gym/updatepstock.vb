﻿Public Class updatepstock
    Dim da As New OleDb.OleDbDataAdapter
    Dim ds As New DataSet
    Dim ds1 As New DataSet

    Private Sub updatepstock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dr As DataRow

        da = New OleDb.OleDbDataAdapter("select pid from product", con)
        ds.Clear()
        da.Fill(ds)

        Dim x As Integer
        While x < ds.Tables(0).Rows.Count
            dr = ds.Tables(0).Rows(x)
            ComboBox1.Items.Add(dr(0))
            x = x + 1
        End While
    End Sub
    Private Sub ComboBox1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Leave
        Dim dr As DataRow

        da = New OleDb.OleDbDataAdapter("select pname,pqty from product where pid=" & CInt(ComboBox1.Text) & "", con)
        ds1.Clear()
        da.Fill(ds1)

        If ds1.Tables(0).Rows.Count > 0 Then
            dr = ds1.Tables(0).Rows(0)
            txtpname.Text = dr(0)
            lblstk.Text = dr(1)
            btnSave.Enabled = True


        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtqty.Text <> "" Then
            Module1.inupdel("update product set pqty=pqty+" & CInt(txtqty.Text) & " where pid=" & CInt(ComboBox1.Text) & "")
            MsgBox("Quantity Updated")
            btnSave.Enabled = False
            Dim dr As DataRow

            da = New OleDb.OleDbDataAdapter("select pname,pqty from product where pid=" & CInt(ComboBox1.Text) & "", con)
            ds1.Clear()
            da.Fill(ds1)

            If ds1.Tables(0).Rows.Count > 0 Then
                dr = ds1.Tables(0).Rows(0)
                txtpname.Text = dr(0)
                lblstk.Text = dr(1)
               

            End If

        End If
    End Sub
End Class