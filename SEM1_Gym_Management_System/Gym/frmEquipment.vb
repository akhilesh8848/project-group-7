﻿Public Class frmEquipment

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            PictureBox1.Image = Image.FromFile(OpenFileDialog1.FileName)
            txtimgpath.Text = OpenFileDialog1.FileName

        End If

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Module1.scalarquery("select max(pid)+1 from Equipment")
        txtid.Text = Module1.nb
        btnSave.Enabled = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtid.Text <> "" And txtimgpath.Text <> "" And txtpname.Text <> "" And txtprice.Text <> "" And txtqty.Text <> "" Then
            Module1.inupdel("insert into Equipment values(" & CInt(txtid.Text) & ",'" & txtpname.Text & "','" & txtprice.Text & "'," & CInt(txtqty.Text) & ",'" & txtimgpath.Text & "')")
            MsgBox("Equipment Added")
            txtid.Clear()
            txtimgpath.Clear()
            txtpname.Clear()
            txtprice.Clear()
            txtqty.Clear()
            PictureBox1.Image = Nothing
            btnSave.Enabled = False

        Else
            MsgBox("Enter All The Fields")
        End If

    End Sub

    Private Sub frmEquipment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.StartPosition = FormStartPosition.CenterScreen

    End Sub
End Class