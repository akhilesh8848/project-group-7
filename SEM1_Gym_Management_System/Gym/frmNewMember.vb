﻿Public Class frmNewMember
    Dim da As New OleDb.OleDbDataAdapter
    Dim ds As New DataSet

    Private Sub txtmmob_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmmob.KeyPress
        If e.KeyChar >= "0" And e.KeyChar <= "9" Or e.KeyChar = Convert.ToChar(Keys.Back) Then

        Else
            e.KeyChar = ""
            txtmmob.Select()
            MsgBox("Enter only Numbers", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Input")
        End If

    End Sub

    Private Sub txtmmob_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles txtmmob.Layout

    End Sub


    Private Sub txtmmob_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtmmob.Leave
        Try

            If txtmmob.Text.Substring(0, 1) >= 7 And txtmmob.Text.Length = 10 Then

            Else

                txtmmob.Select()
                MsgBox("Enter valid mobile", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Input")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        Dim dr As DataRow

        If txtmmob.Text <> "" Then
            da = New OleDb.OleDbDataAdapter("select * from GymMember where cmob='" & txtmmob.Text & "'", con)
            ds.Clear()
            da.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                dr = ds.Tables(0).Rows(0)
                txtmname.Text = dr(1)
                txtadd.Text = dr(2)
                txtecon.Text = dr(3)
                cmbRel.Text = dr(4)
                txthealth.Text = dr(5)
                txtsubstatus.Text = dr(6)
                btnUpdate.Enabled = True
                btnSave.Enabled = False

            Else

                txtmname.Text = "" ' dr(1)
                txtadd.Text = "" 'dr(2)"
                txtecon.Text = "" ' dr(3)
                cmbRel.Text = "" 'dr(4)
                txthealth.Text = "" 'dr(5)
                txtsubstatus.Text = "InActive" ' dr(6)
                btnUpdate.Enabled = False
                btnSave.Enabled = True

            End If
        End If
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtmmob.Text <> "" And txtadd.Text <> "" And txtecon.Text <> "" And txthealth.Text <> "" And txtmname.Text <> "" And txtsubstatus.Text <> "" And cmbRel.Text <> "" Then
            Module1.inupdel("insert into GymMember values('" & txtmmob.Text & "','" & txtmname.Text & "','" & txtadd.Text & "','" & txtecon.Text & "','" & cmbRel.Text & "','" & txthealth.Text & "','" & txtsubstatus.Text & "')")
            MsgBox("New Member Added")
            txtmname.Text = "" ' dr(1)
            txtadd.Text = "" 'dr(2)"
            txtecon.Text = "" ' dr(3)
            cmbRel.Text = "" 'dr(4)
            txthealth.Text = "" 'dr(5)
            txtsubstatus.Text = "" ' dr(6)
        Else
            MsgBox("Fill All The Fields")
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim ch As Integer
        ch = MsgBox("Really Want to Update member Details?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Update")
        If ch = vbYes Then
            If txtmmob.Text <> "" And txtadd.Text <> "" And txtecon.Text <> "" And txthealth.Text <> "" And txtmname.Text <> "" And txtsubstatus.Text <> "" And cmbRel.Text <> "" Then
                Module1.inupdel("update GymMember set cadd='" & txtadd.Text & "',cecontact='" & txtecon.Text & "',relwithcust='" & cmbRel.Text & "',healthhis='" & txthealth.Text & "'")
                MsgBox("Member Details Updated")
                txtmname.Text = "" ' dr(1)
                txtadd.Text = "" 'dr(2)"
                txtecon.Text = "" ' dr(3)
                cmbRel.Text = "" 'dr(4)
                txthealth.Text = "" 'dr(5)
                txtsubstatus.Text = "" ' dr(6)


            Else
                MsgBox("Fields Should not be Empty")

            End If
        End If

    End Sub

    Private Sub txtmmob_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtmmob.TextChanged

    End Sub

    Private Sub txtmname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmname.KeyPress
        If e.KeyChar >= "a" And e.KeyChar <= "z" Or e.KeyChar >= "A" And e.KeyChar <= "Z" Or e.KeyChar = " " Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            'nothing to do valid character
        Else
            e.KeyChar = ""
            txtmname.Select()
            MsgBox("Enter only Alphabets", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Input")
        End If

    End Sub

   
End Class