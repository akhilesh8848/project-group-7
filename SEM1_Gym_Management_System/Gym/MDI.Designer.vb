﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.PackageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CreateNewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewAllPackagesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MemberToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddNewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ManageSubscriptionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ListMembersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TrainerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddUpdateTrainerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewTrainersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewAllEmployeesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UpdateProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UpdateStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EquipmentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddNewToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CountEquipmentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ReceiptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DuplicateReceiptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewReceiptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BillToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MemberReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SubscriptionReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EmployeesReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SupplierReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ProductReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PackageToolStripMenuItem, Me.MemberToolStripMenuItem, Me.TrainerToolStripMenuItem, Me.ProductToolStripMenuItem, Me.SupplierToolStripMenuItem, Me.EquipmentToolStripMenuItem, Me.ReceiptToolStripMenuItem, Me.BillToolStripMenuItem, Me.ReportsToolStripMenuItem, Me.LogoutToolStripMenuItem, Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(696, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'PackageToolStripMenuItem
        '
        Me.PackageToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CreateNewToolStripMenuItem, Me.ViewAllPackagesToolStripMenuItem})
        Me.PackageToolStripMenuItem.Name = "PackageToolStripMenuItem"
        Me.PackageToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.PackageToolStripMenuItem.Text = "Package"
        '
        'CreateNewToolStripMenuItem
        '
        Me.CreateNewToolStripMenuItem.Name = "CreateNewToolStripMenuItem"
        Me.CreateNewToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.CreateNewToolStripMenuItem.Text = "Create New"
        '
        'ViewAllPackagesToolStripMenuItem
        '
        Me.ViewAllPackagesToolStripMenuItem.Name = "ViewAllPackagesToolStripMenuItem"
        Me.ViewAllPackagesToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ViewAllPackagesToolStripMenuItem.Text = "View All Packages"
        '
        'MemberToolStripMenuItem
        '
        Me.MemberToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddNewToolStripMenuItem, Me.ManageSubscriptionToolStripMenuItem, Me.ListMembersToolStripMenuItem})
        Me.MemberToolStripMenuItem.Name = "MemberToolStripMenuItem"
        Me.MemberToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.MemberToolStripMenuItem.Text = "Member"
        '
        'AddNewToolStripMenuItem
        '
        Me.AddNewToolStripMenuItem.Name = "AddNewToolStripMenuItem"
        Me.AddNewToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.AddNewToolStripMenuItem.Text = "Search / Add"
        '
        'ManageSubscriptionToolStripMenuItem
        '
        Me.ManageSubscriptionToolStripMenuItem.Name = "ManageSubscriptionToolStripMenuItem"
        Me.ManageSubscriptionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.ManageSubscriptionToolStripMenuItem.Text = "Manage Subscription"
        '
        'ListMembersToolStripMenuItem
        '
        Me.ListMembersToolStripMenuItem.Name = "ListMembersToolStripMenuItem"
        Me.ListMembersToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.ListMembersToolStripMenuItem.Text = "Check Membership"
        '
        'TrainerToolStripMenuItem
        '
        Me.TrainerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddUpdateTrainerToolStripMenuItem, Me.ViewTrainersToolStripMenuItem, Me.ViewAllEmployeesToolStripMenuItem})
        Me.TrainerToolStripMenuItem.Name = "TrainerToolStripMenuItem"
        Me.TrainerToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.TrainerToolStripMenuItem.Text = "Employee"
        '
        'AddUpdateTrainerToolStripMenuItem
        '
        Me.AddUpdateTrainerToolStripMenuItem.Name = "AddUpdateTrainerToolStripMenuItem"
        Me.AddUpdateTrainerToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.AddUpdateTrainerToolStripMenuItem.Text = "New Employee"
        '
        'ViewTrainersToolStripMenuItem
        '
        Me.ViewTrainersToolStripMenuItem.Name = "ViewTrainersToolStripMenuItem"
        Me.ViewTrainersToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.ViewTrainersToolStripMenuItem.Text = "View Trainers"
        '
        'ViewAllEmployeesToolStripMenuItem
        '
        Me.ViewAllEmployeesToolStripMenuItem.Name = "ViewAllEmployeesToolStripMenuItem"
        Me.ViewAllEmployeesToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.ViewAllEmployeesToolStripMenuItem.Text = "View All Employees"
        '
        'ProductToolStripMenuItem
        '
        Me.ProductToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewProductToolStripMenuItem, Me.UpdateProductToolStripMenuItem, Me.UpdateStockToolStripMenuItem})
        Me.ProductToolStripMenuItem.Name = "ProductToolStripMenuItem"
        Me.ProductToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ProductToolStripMenuItem.Text = "Product"
        '
        'NewProductToolStripMenuItem
        '
        Me.NewProductToolStripMenuItem.Name = "NewProductToolStripMenuItem"
        Me.NewProductToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.NewProductToolStripMenuItem.Text = "New Product"
        '
        'UpdateProductToolStripMenuItem
        '
        Me.UpdateProductToolStripMenuItem.Name = "UpdateProductToolStripMenuItem"
        Me.UpdateProductToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.UpdateProductToolStripMenuItem.Text = "Update Product"
        '
        'UpdateStockToolStripMenuItem
        '
        Me.UpdateStockToolStripMenuItem.Name = "UpdateStockToolStripMenuItem"
        Me.UpdateStockToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.UpdateStockToolStripMenuItem.Text = "Update / Check Stock"
        '
        'SupplierToolStripMenuItem
        '
        Me.SupplierToolStripMenuItem.Name = "SupplierToolStripMenuItem"
        Me.SupplierToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.SupplierToolStripMenuItem.Text = "Supplier"
        '
        'EquipmentToolStripMenuItem
        '
        Me.EquipmentToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddNewToolStripMenuItem1, Me.CountEquipmentToolStripMenuItem})
        Me.EquipmentToolStripMenuItem.Name = "EquipmentToolStripMenuItem"
        Me.EquipmentToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.EquipmentToolStripMenuItem.Text = "Equipment"
        '
        'AddNewToolStripMenuItem1
        '
        Me.AddNewToolStripMenuItem1.Name = "AddNewToolStripMenuItem1"
        Me.AddNewToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.AddNewToolStripMenuItem1.Text = "Add New"
        '
        'CountEquipmentToolStripMenuItem
        '
        Me.CountEquipmentToolStripMenuItem.Name = "CountEquipmentToolStripMenuItem"
        Me.CountEquipmentToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.CountEquipmentToolStripMenuItem.Text = "Count Equipment"
        '
        'ReceiptToolStripMenuItem
        '
        Me.ReceiptToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DuplicateReceiptToolStripMenuItem, Me.NewReceiptToolStripMenuItem})
        Me.ReceiptToolStripMenuItem.Name = "ReceiptToolStripMenuItem"
        Me.ReceiptToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.ReceiptToolStripMenuItem.Text = "Receipt"
        Me.ReceiptToolStripMenuItem.Visible = False
        '
        'DuplicateReceiptToolStripMenuItem
        '
        Me.DuplicateReceiptToolStripMenuItem.Name = "DuplicateReceiptToolStripMenuItem"
        Me.DuplicateReceiptToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.DuplicateReceiptToolStripMenuItem.Text = "Duplicate Receipt"
        '
        'NewReceiptToolStripMenuItem
        '
        Me.NewReceiptToolStripMenuItem.Enabled = False
        Me.NewReceiptToolStripMenuItem.Name = "NewReceiptToolStripMenuItem"
        Me.NewReceiptToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.NewReceiptToolStripMenuItem.Text = "New Receipt"
        '
        'BillToolStripMenuItem
        '
        Me.BillToolStripMenuItem.Name = "BillToolStripMenuItem"
        Me.BillToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.BillToolStripMenuItem.Text = "Bill"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MemberReportToolStripMenuItem, Me.SubscriptionReportToolStripMenuItem, Me.EmployeesReportToolStripMenuItem, Me.SupplierReportToolStripMenuItem, Me.ProductReportToolStripMenuItem})
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'MemberReportToolStripMenuItem
        '
        Me.MemberReportToolStripMenuItem.Name = "MemberReportToolStripMenuItem"
        Me.MemberReportToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.MemberReportToolStripMenuItem.Text = "Member Report"
        '
        'SubscriptionReportToolStripMenuItem
        '
        Me.SubscriptionReportToolStripMenuItem.Name = "SubscriptionReportToolStripMenuItem"
        Me.SubscriptionReportToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.SubscriptionReportToolStripMenuItem.Text = "Subscription Report"
        Me.SubscriptionReportToolStripMenuItem.Visible = False
        '
        'EmployeesReportToolStripMenuItem
        '
        Me.EmployeesReportToolStripMenuItem.Name = "EmployeesReportToolStripMenuItem"
        Me.EmployeesReportToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.EmployeesReportToolStripMenuItem.Text = "Employees Report"
        '
        'SupplierReportToolStripMenuItem
        '
        Me.SupplierReportToolStripMenuItem.Name = "SupplierReportToolStripMenuItem"
        Me.SupplierReportToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.SupplierReportToolStripMenuItem.Text = "Supplier Report"
        Me.SupplierReportToolStripMenuItem.Visible = False
        '
        'ProductReportToolStripMenuItem
        '
        Me.ProductReportToolStripMenuItem.Name = "ProductReportToolStripMenuItem"
        Me.ProductReportToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ProductReportToolStripMenuItem.Text = "Product Report"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(12, 20)
        '
        'MDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Gym.My.Resources.Resources.dumb
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(696, 473)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MDI"
        Me.Text = "Gym Management System"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MemberToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddNewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManageSubscriptionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListMembersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrainerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddUpdateTrainerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewTrainersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateStockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpdateProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewAllEmployeesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EquipmentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddNewToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CountEquipmentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReceiptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DuplicateReceiptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewReceiptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MemberReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SubscriptionReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmployeesReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SupplierReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateNewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewAllPackagesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BillToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
