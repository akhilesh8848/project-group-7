﻿Public Class frmEquipCount
    Dim da As New OleDb.OleDbDataAdapter
    Dim ds As New DataSet
    Dim ds1 As New DataSet
    Dim x As Integer
    Dim dr As DataRow
    Private Sub frmEquipCount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        x = 0
        da = New OleDb.OleDbDataAdapter("select pname,pqty from equipment", con)
        ds.Clear()
        da.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            dr = ds.Tables(0).Rows(x)
            Call display()
        End If

    End Sub
    Private Sub display()
        Label4.Text = dr(0)
        lblstk.Text = dr(1)

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        x = x + 1
        If x < ds.Tables(0).Rows.Count Then
            dr = ds.Tables(0).Rows(x)
            Call display()
        Else
            MsgBox("This is Last Equipment")
            x = x - 1
            dr = ds.Tables(0).Rows(x)
            Call display()

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        x = x - 1
        If x >= 0 Then
            dr = ds.Tables(0).Rows(x)
            Call display()
        Else
            MsgBox("This is First Equipment")
            x = x + 1
            dr = ds.Tables(0).Rows(x)
            Call display()

        End If
    End Sub
End Class