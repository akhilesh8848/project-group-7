﻿Public Class updateproduct
    Dim da As New OleDb.OleDbDataAdapter
    Dim ds As New DataSet
    Dim ds1 As New DataSet

    Private Sub updateproduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.StartPosition = FormStartPosition.CenterScreen

        da = New OleDb.OleDbDataAdapter("select * from product", con)
        ds.Clear()
        da.Fill(ds)
        DataGridView1.DataSource = ds.Tables(0)



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dr As DataRow
        da = New OleDb.OleDbDataAdapter("select * from product where pid=" & CInt(txtid.Text) & "", con)
        ds1.Clear()
        da.Fill(ds1)
        If ds1.Tables(0).Rows.Count > 0 Then
            DataGridView1.DataSource = ds.Tables(0)
            dr = ds1.Tables(0).Rows(0)
            txtpname.Text = dr(1)
            txtpcomp.Text = dr(2)
            txtprice.Text = dr(3)
            txtqty.Text = dr(4)
            btnUpdate.Enabled = True
        Else
            MsgBox("Product Id You Entered Is Incorrect")
            txtpcomp.Clear()
            txtpname.Clear()
            txtprice.Clear()
            txtqty.Clear()
            txtid.Select()


        End If
    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If txtprice.Text <> "" Then
            Module1.inupdel("update product set pprice='" & txtprice.Text & "' where pid=" & CInt(txtid.Text) & "")
            MsgBox("Price Updated")
            da = New OleDb.OleDbDataAdapter("select * from product", con)
            ds.Clear()
            da.Fill(ds)
            DataGridView1.DataSource = ds.Tables(0)
        Else
            MsgBox("Enter Price")
            txtprice.Select()
        End If
    End Sub
End Class