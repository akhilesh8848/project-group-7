<!--
    
    Project Name: E-Airtel
-->
<%@ page language="java" %>
<%@include file="jsp-pages/common-database.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Site made with Mobirise Website Builder v4.7.6, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v4.7.6, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/airtel-logo-red-text-horizontal-122x50.jpg" type="image/x-icon">
        <meta name="description" content="Web Creator Description">
        <title>Postpaid | Edit</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/soundcloud-plugin/style.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

        <!-- Start: Validation scripts-->
        <script src="validation-scripts/customer-validation-script.js"> </script>
        <!-- End: Validation scripts-->
    </head>
    <body>
        <section class="menu cid-qTCFVt5HNb" once="menu" id="menu1-e">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                        <a href="">
                        <img src="assets/images/airtel-logo-red-text-horizontal-122x50.jpg" alt="Mobirise" title="" style="height: 3.8rem;">
                        </a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="">E-Airtel</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="https://mobirise.com"><span class="mbri-unlock mbr-iconfont mbr-iconfont-btn"></span>
                            Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="engine"><a href="https://mobirise.me/y">free html website templates</a></section>
        <section class="mbr-section form1 cid-qTC9zerAL5 mbr-parallax-background" id="form1-6">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="title col-12 col-lg-8">
                        <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2"><strong>
                            EDIT CUSTOMER DETAILS
                            </strong>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="media-container-column col-lg-8">
                        <form class="mbr-form" action="jsp-pages/edit-postpaid-customer-details.jsp" method="post">
                            <!-- <input type="hidden" name="email" data-form-email="true" value="mynH+cwVa6zvLNJfmNg4Oslwi0XLVzTAPkHQP/jps1Brvwi1/ydDtHG1QeB6Spl8PwpALE0ZIZwY4rs3TZYwFpRpniDLRjU7RtSFsagAvlZYy0IwJ7WkDtAj0ZYxJgxz" data-form-field="Email"> -->
                            
                            <%
                                String contactNo = request.getParameter("contactNo");
                                try{
                                    con = fetchConnection();
                                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                                    String query = "select * from postpaid where postphno = '"+contactNo+"'";
                                    rs = stmt.executeQuery(query);
                                    rs.next();
                                    
                                    String postFirstName = rs.getString("postfname");
                                    String postMiddleName = rs.getString("postmname");
                                    String postLastName = rs.getString("postlname");
                                    String postAddress = rs.getString("postadd");
                                    String postCity = rs.getString("postcity");
                                    String postState = rs.getString("poststate");
                                    String postBirthDate = rs.getString("postbirthdate");
                                    String postIssueDate = rs.getString("postissue");
                                    String postSimNo = rs.getString("postsimno");
                                    String postGender = rs.getString("postgender");
                                    
                            %>
                            
                            <input type="hidden" name="contact_no" value="<%=rs.getString("postphno")%>">
                            <div class="row row-sm-offset">
                                <div class="col-md-4 multi-horizontal" data-for="name">
                                    <div class="form-group">
                                        <input type="text" placeholder="First Name" value="<%=postFirstName%>" class="form-control" name="first_name" data-form-field="Name" required="" id="first_name" onchange="firstNameValidate()">
                                    </div>
                                </div>
                                <div class="col-md-4 multi-horizontal" data-for="name">
                                    <div class="form-group">
                                        <input type="text" placeholder="Middle Name" class="form-control" value="<%=postMiddleName%>" name="middle_name" data-form-field="Name" required="" id="middle_name" onchange="middleNameValidate()">
                                    </div>
                                </div>
                                <div class="col-md-4 multi-horizontal" data-for="phone">
                                    <div class="form-group">
                                        <input type="tel" placeholder="Last Name" class="form-control" value="<%=postLastName%>" name="last_name" data-form-field="Name" id="last_name" onchange="lastNameValidate()">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group" data-for="message">
                                <textarea type="text" placeholder="Address" class="form-control" name="address" rows="3" data-form-field="Message" id="address" onchange="addressValidation()"><%=postAddress%></textarea>
                            </div>
                            
                            <div class="row row-sm-offset">
                                <div class="col-md-4 multi-horizontal" data-for="name">
                                    <div class="form-group">
                                        <input type="text" placeholder="City" value="<%=postCity%>" class="form-control" name="city" data-form-field="Name" required="" id="city" onchange="cityValidation()">
                                    </div>
                                </div>
                                <div class="col-md-4 multi-horizontal" data-for="email">
                                    <div class="form-group">
                                        <input type="text" placeholder="State" value="<%=postState%>" class="form-control" name="state" data-form-field="Name" required="" id="state" onchange="stateValidation()">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row row-sm-offset">
                                <div class="col-md-4 multi-horizontal" data-for="name">
                                    <div class="form-group">
                                        <input type="text" placeholder="Gender" value="<%=postGender%>" class="form-control" name="gender" data-form-field="Name" required="" id="gender" onchange="genderValidation()">
                                    </div>
                                </div>
                                <div class="col-md-4 multi-horizontal" data-for="email">
                                    <div class="form-group">
                                        <input type="text" placeholder="DOB (yyyy/mm/dd)" value="<%=postBirthDate%>" class="form-control" name="date_of_birth" data-form-field="Name" required="" id="date_of_birth" onchange="birthDateValidation()">
                                    </div>
                                </div>
                                <div class="col-md-4 multi-horizontal" data-for="phone">
                                    <div class="form-group">
                                        <input type="text" placeholder="Issue Date (yyyy/mm/dd)" value="<%=postIssueDate%>" class="form-control" name="issue_date" data-form-field="Name" id="issue_date" onchange="issueDateValidate()">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" data-for="message">
                                <input type="text" placeholder="SIM Serial Number" value="<%=postSimNo%>" class="form-control" name="sim_no" rows="3" data-form-field="Message" id="sim_no" maxlength="15" onchange="serialNumberValidation()"></input>
                            </div>

                        <%
                                } catch(Exception e){
                                    out.println(" Contact Exception: " +e);
                                } finally{
                                    closeConnection();
                                }
                        %>
                            <span class="input-group-btn">
                            <button href="" type="submit" class="btn btn-primary btn-form display-4">MODIFY</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/script.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/theme/js/script.js"></script>
        <script src="assets/formoid/formoid.min.js"></script>
    </body>
</html>