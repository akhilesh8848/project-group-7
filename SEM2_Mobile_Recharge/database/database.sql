create table register(
name varchar(100) unique,
email varchar(50),
username varchar(20),
password varchar(20)
);

create table prepaid(
prefname varchar(20),
premname varchar(20),
prelname varchar(20),
preadd varchar(100),
precity varchar(20),
prestate varchar(20),
prebirthdate varchar(20),
prephno varchar(20),
presimno varchar(20),
preissue varchar(20),
pregender varchar(20),
preno int primary key AUTO_INCREMENT,
constraint phno_uk unique(prephno));

create table postpaid(
postfname varchar(20),
postmname varchar(20),
postlname varchar(20),
postadd varchar(100),
postcity varchar(20),
poststate varchar(20),
postbirthdate varchar(20),
postphno varchar(20),
postsimno varchar(20),
postissue varchar(20),
postgender varchar(20),
postno int primary key AUTO_INCREMENT,
constraint pphno_uk unique(postphno));