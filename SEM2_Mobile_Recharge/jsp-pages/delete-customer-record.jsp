<%@ page language="java"%>
<%@include file="common-database.jsp" %>

<%
    String contactNo = request.getParameter("contact_no");
    String prepaid = request.getParameter("prepaid");
    String postpaid = request.getParameter("postpaid");

    boolean flag = false;
    try{
        if(prepaid.equalsIgnoreCase("yes")){
            flag = deleteCustomerRecord(out, contactNo, true);
            if(flag){
                out.println("<br><br><br><br><br><br><center> <font size=5 color=green> <b> Record successfully removed! </b> </font> </center>");
            } else {
                out.println("<br><br><br><br><br><br><center> <font size=5 color=red> <b> There is error occured while deleting! </b> </font> </center>");
            }            
        } else if(postpaid.equalsIgnoreCase("yes")){
            flag = deleteCustomerRecord(out, contactNo, false);
            if(flag){
                out.println("<br><br><br><br><br><br><center> <font size=5 color=green> <b> Record successfully removed! </b> </font> </center>");
            } else {
                out.println("<br><br><br><br><br><br><center> <font size=5 color=red> <b> There is error occured while deleting! </b> </font> </center>");
            }
        }
    } catch(Exception e){
        out.println(" Exception: " +e);
    }
%>

<%!
    public static boolean deleteCustomerRecord(JspWriter out, String contactNo, boolean flag) throws Exception{
        boolean transactionFlag = false;
        try{
            String query = "";
            con = fetchConnection();
            stmt = con.createStatement();
            if(flag){
                query = "delete from prepaid where prephno = '"+contactNo+"'";
            } else {
                query = "delete from postpaid where postphno = '"+contactNo+"'";
            }
            int ans = stmt.executeUpdate(query);
            if(ans != 0){
                transactionFlag = true;
            }
        } catch(Exception e){
            out.println(" Delete customer record: " +e);
            transactionFlag = false;
        } finally{
            closeConnection();
        }
        return transactionFlag;
    }
%>