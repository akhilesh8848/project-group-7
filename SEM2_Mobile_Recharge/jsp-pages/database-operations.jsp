<%@ page language="java" import="java.sql.*, java.util.*, java.time.*"%>
<%@include file="common-database.jsp" %>

<%!
    public static String generatePrimaryKey(String startingCharacter, String tableName, String primaryKey, JspWriter out) throws Exception{
        String id = "";
        try{
            con = fetchConnection();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String query = "select "+primaryKey+" from "+tableName+"";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                id = startingCharacter+"1";
            } else {
                rs.last();
                int intId = Integer.parseInt(rs.getString(primaryKey).split(startingCharacter)[1]);
                intId++;
                id = startingCharacter+intId;
            }
        } catch(Exception e){
            out.println(" Generate primary key exception: " +e);
        } finally {
            closeConnection();
        }
        return id;
    }
%>