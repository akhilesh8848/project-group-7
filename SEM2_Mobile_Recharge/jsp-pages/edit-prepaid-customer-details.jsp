<%@ page language="java" %>
<%@include file="common-database.jsp" %>

<%
    String firstName = request.getParameter("first_name");
    String middleName = request.getParameter("middle_name");
    String lastName = request.getParameter("last_name");
    String address = request.getParameter("address");
    String city = request.getParameter("city");
    String state= request.getParameter("state");
    String birthDate = request.getParameter("date_of_birth");
    String issueDate = request.getParameter("issue_date");
    String simSerialNo = request.getParameter("sim_no");
    String gender = request.getParameter("gender");
    String contactNo = request.getParameter("contact_no");

    boolean flag = updatePrepaidCustomerDetails(out, firstName, middleName, lastName, contactNo, address, city, state, birthDate, issueDate, simSerialNo, gender);
    if(flag){
        %>
            <br><br><br><br><br><br><br>
            <center>
                <font size="6" color="green">
                    <b>Contact details updated successfully!</b> <br>
                <font>
                <a href="../option-customer-record.html">Click here to go back...</a>
            </center>
        <%
    } else {
        out.println(" <br><br><br><br><br> <center> <font size=6 color=red> <b> There was an error occurred while updating! </b> </font> </center> ");
    }
%>

<%!
    public static boolean updatePrepaidCustomerDetails(JspWriter out, String firstName, String middleName, String lastName, String contactNo, String address, String city, String state, String birthDate, String issueDate, String simSerialNo, String gender) throws Exception{
        boolean transactionFlag = false;
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String updateQuery = "update prepaid set prefname = '"+firstName+"', premname = '"+middleName+"', prelname = '"+lastName+"', preadd = '"+address+"', precity = '"+city+"', prestate = '"+state+"', prebirthdate = '"+birthDate+"', presimno = '"+simSerialNo+"', preissue = '"+issueDate+"', pregender = '"+gender+"' where prephno = '"+contactNo+"'";
            stmt.executeUpdate(updateQuery);
            transactionFlag = true;
        } catch (Exception e){
            out.println(" Update Exception: " +e);
        } finally {
        }
        return transactionFlag;
    }
%>