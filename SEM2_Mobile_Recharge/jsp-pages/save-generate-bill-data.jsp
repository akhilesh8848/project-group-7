<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    String customerId = request.getParameter("customerId");
    String planNo = request.getParameter("planNo");
    String rechargeId = request.getParameter("rechargeId");
    String mobileNo = request.getParameter("mobileNo");
    String billAmount = request.getParameter("billAmount");
    
    //out.println(customerId + " " + planNo + " " + rechargeId + " " + mobileNo + " " + billAmount);
    
    List<String> billData = new ArrayList<String>();
    billData.add(customerId);
    billData.add(planNo);
    billData.add(rechargeId);
    billData.add(mobileNo);
    billData.add(billAmount);

    try{
        updateRechageAmount(billData, out);
        boolean flag = saveGeneratedBillData(billData, out);
        if(flag){
            out.println(" Bill successfully generated!");
        } else {
            out.println(" There is an error occurred!");
        }
    } catch(Exception e){
        out.println(" Save generate bill data exception: " +e);
    }
%>

<%!
    public static boolean saveGeneratedBillData(List<String> billData, JspWriter out) throws Exception{
        boolean flag = false;
        try{
            String postpaidBillNo = generatePrimaryKey("B", "postpaid_bill", "bill_no", out);
            double amount = Double.parseDouble(billData.get(4));
            String date = LocalDate.now().toString();
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "insert into postpaid_bill values('"+postpaidBillNo+"', "+amount+", '"+date+"', '"+billData.get(3)+"', '"+billData.get(1)+"', '"+billData.get(0)+"')";
            out.println(query);
            stmt.executeUpdate(query);
            flag = true;
        } catch(Exception e){
            out.println(" Save generated bill method data exception: " +e);
        } finally {
            closeConnection();
        }
        return flag;
    }
%>

<%!
    public static void updateRechageAmount(List<String> billData, JspWriter out) throws Exception {
        try{
            double previousAmount = getPreviousAmount(billData, out);
            //double latestAmount = previousAmount + Double.parseDouble(billData.get(4));
            double latestAmount = Double.parseDouble(billData.get(4));
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "update recharge set recharge_amount = "+latestAmount+" where recharge_no = '"+billData.get(2)+"'";
            stmt.executeUpdate(query);
        } catch(Exception e){
            out.println(" Update recharge amount exception: " +e);
        } finally {
            closeConnection();
        }
    }
%>

<%!
    public static double getPreviousAmount(List<String> billData, JspWriter out) throws Exception{
        double amount = 0.0;
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select recharge_amount from recharge where recharge_no = '"+billData.get(2)+"'";
            rs = stmt.executeQuery(query);
            rs.next();
            amount = Double.parseDouble(rs.getString("recharge_amount"));
        } catch(Exception e){
            out.println(" Get previous amount exception: " +e);
        } finally {
            closeConnection();            
        }
        return amount;
    }
%>