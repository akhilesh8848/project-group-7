<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    String fullName = request.getParameter("name");
    String emailId = request.getParameter("email");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    
    try{
        String customerId = storeDetailsIntoDB(fullName, emailId, username, password, out);
        if(customerId != null){
            response.sendRedirect("../customer-check-plan.html?customerId="+customerId);
        }
    } catch(Exception e){
        out.println(" Store database exception: " +e);
    }
%>

<%!
    public String storeDetailsIntoDB(String fullName, String emailId, String username, String password, JspWriter out) throws Exception{
        String customerId = generatePrimaryKey("C", "customer_login", "cust_id", out);
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            
            String query = "insert into customer_login values('"+customerId+"', '"+fullName+"', '"+emailId+"', '"+username+"','"+password+"')";
            stmt.executeUpdate(query);
        } catch(Exception e){
            String printError = "<html>"+
                                "<center><br><br><br><br>"+
                                    "<table>"+
                                        "<tr>"+
                                            "<td> <center> <font size=5 color=red> <b> Invalid Record </b> </font> </center> </td>"+
                                        "</tr>"+
                                        "<tr>"+
                                            "<td>"+e+"</td>"+
                                        "</tr>"+
                                    "</table>"+
                                "</center>"+
                           "</html>";
            out.println(printError);
        } finally{
            closeConnection();
        }
        return customerId;
    }
%>