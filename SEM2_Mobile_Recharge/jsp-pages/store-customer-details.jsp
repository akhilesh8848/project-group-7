<%@page language="java" import="java.sql.*"%>
<%@include file="common-database.jsp" %>

<%
    String firstName = request.getParameter("first_name");
    String middleName = request.getParameter("middle_name");
    String lastName = request.getParameter("last_name");
    
    String address = request.getParameter("address");
    String city = request.getParameter("city");
    String state = request.getParameter("state");

    String birthDate = request.getParameter("date_of_birth");
    String issueDate = request.getParameter("issue_date");
    String contactNo = request.getParameter("contact_no");
    
    String simSerialNo = request.getParameter("sim_no");
    String gender = request.getParameter("gender");
    String prepaid = request.getParameter("prepaid");
    String postpaid = request.getParameter("postpaid");
    
    boolean flag = false;
    if(prepaid.equalsIgnoreCase("yes")){
        flag = storeCustomerDetails(out, firstName, middleName, lastName, address, city, state, birthDate, issueDate, contactNo, simSerialNo, gender, true);
        if(flag){
            response.sendRedirect("../goto-back-page.html");
        } else {
            response.sendRedirect("../error-page.html");
        }
    } else if(postpaid.equalsIgnoreCase("yes")){
        flag = storeCustomerDetails(out, firstName, middleName, lastName, address, city, state, birthDate, issueDate, contactNo, simSerialNo, gender, false);
        if(flag){
            response.sendRedirect("../goto-back-page.html");
        } else {
            response.sendRedirect("../error-page.html");
        }
    } else {
        out.println(" Not selected prepaid / postpaid");
    }
%>

<%!
    public static boolean storeCustomerDetails(JspWriter out, String firstName, String middleName, String lastName, String address, String city, String state, String birthDate, String issueDate, String contactNo, String simSerialNo, String gender, boolean flag) throws Exception{
        boolean transactionFlag = false;
        try{
            String query = "";
            if(flag){
                query = "insert into prepaid(prefname, premname, prelname, preadd, precity, prestate, prebirthdate, prephno, presimno, preissue, pregender) values('"+firstName+"','"+middleName+"','"+lastName+"','"+address+"','"+city+"','"+state+"','"+birthDate+"','"+contactNo+"','"+simSerialNo+"','"+issueDate+"','"+gender+"')";
            } else {
                query = "insert into postpaid(postfname, postmname, postlname, postadd, postcity, poststate, postbirthdate, postphno, postsimno, postissue, postgender) values('"+firstName+"','"+middleName+"','"+lastName+"','"+address+"','"+city+"','"+state+"','"+birthDate+"','"+contactNo+"','"+simSerialNo+"','"+issueDate+"','"+gender+"')";
            }
            
            con = fetchConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            transactionFlag = true;
        } catch(Exception e){
            out.println(" Store customer exception: " +e);
        } finally{
            closeConnection();
        }
        return transactionFlag;
    }
%>