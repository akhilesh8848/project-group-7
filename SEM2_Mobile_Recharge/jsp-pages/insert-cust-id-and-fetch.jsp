<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
	String flag = request.getParameter("flag");
	String tableName = "";
	String prefix = "";
	String primaryKeyName = "";
	
	if(flag.equals("prepaid")){
		tableName = "prepaid";
		prefix = "PR";
		primaryKeyName = "prepaid_id";
	} else if(flag.equals("postpaid")){
		tableName = "postpaid";
		prefix = "PO";
		primaryKeyName = "postpaid_id";
	}
	
	try{
		String  primaryKey = generatePrimaryKey(prefix, tableName, primaryKeyName, out);
		insertNewId(tableName, primaryKey, primaryKeyName, out);
		out.println(primaryKey);
	} catch(Exception e){
		out.println(" Insert customer id and fetch exception: " +e);
	}
%>

<%!
	public static void insertNewId(String tableName, String primaryKey, String primaryKeyName, JspWriter out) throws Exception{
		try{
			con = fetchConnection();
			stmt = con.createStatement();
			String query = "insert into "+tableName+"("+primaryKeyName+") values('"+primaryKey+"')";
			stmt.executeUpdate(query);
		} catch(Exception e){
			out.println(" Insert new id exception: " +e);
		} finally {
			closeConnection();
		}
	}
%>