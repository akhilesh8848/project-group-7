<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
	String mobileNo = request.getParameter("mobileNo");
	String flag = request.getParameter("flag");
	String tableName = "";
	boolean prepostFlag = false;
	if(flag.equals("prepaid")){
		tableName = "prepaid";
		prepostFlag = true;
	} else {
		tableName = "postpaid";
		prepostFlag = false;
	}
	
	try{
		boolean isUpdated = activateSimCard(mobileNo, tableName, prepostFlag, out);
		if(isUpdated){
			out.println(" Sim activated successfully! ");
		} else {
			response.sendRedirect("../error-page.html");
		}
	} catch(Exception e){
		out.println(" Activate sim card exception: " +e);
	}
%>

<%!
	public static boolean activateSimCard(String mobileNo, String tableName, boolean flag, JspWriter out) throws Exception {
		boolean isUpdated = false;
		try{
			String query = "";
			if(flag){
				query = "update "+tableName+" set activate_sim_status = 1 where prephno = '"+mobileNo+"'";
			} else {
				query = "update "+tableName+" set activate_sim_status = 1 where postphno = '"+mobileNo+"'";
			}
			con = fetchConnection();
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			isUpdated = true;
		} catch(Exception e){
			out.println(" Activate sim card exception: " +e);
		} finally {
			closeConnection();
		}
		return isUpdated;
	}
%>