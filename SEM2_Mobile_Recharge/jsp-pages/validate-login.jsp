<%@page language="java" import="java.sql.*"%>
<%@include file="common-database.jsp" %>

<%
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    
    try{
        String customerId = validateLoginDetails(username, password, out);
        if(customerId != null){
            response.sendRedirect("../customer-check-plan.html?customerId="+customerId);
        } else {
            out.println(" <br><br><br><br> <center> <b> <font size=5 color=red> Invalid username and password </font> </b> </center>");
        }
    } catch(Exception e){
        out.println(" Login Exception: " +e);
    }
%>

<%!
    public static String validateLoginDetails(String username, String password, JspWriter out) throws Exception{
        String customerId = "";
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select cust_id from customer_login where username = '"+username+"' and password = '"+password+"'";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                customerId = null;
            } else {
                customerId = rs.getString("cust_id");
            }
        } catch(Exception e){
            out.println(" Login validity exception: " +e);
        } finally{
            closeConnection();
        }
        return customerId;
    }        
%>