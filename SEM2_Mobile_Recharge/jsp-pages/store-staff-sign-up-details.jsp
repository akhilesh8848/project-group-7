<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    String fullName = request.getParameter("name");
    String emailId = request.getParameter("email");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    
    try{
        boolean flag = storeDetailsIntoDB(fullName, emailId, username, password, out);
        if(flag){
            response.sendRedirect("../option-customer-record.html");
        }
    } catch(Exception e){
        out.println(" Store database exception: " +e);
    }
%>

<%!
    public boolean storeDetailsIntoDB(String fullName, String emailId, String username, String password, JspWriter out) throws Exception{
        boolean flag = false;
        String staffId = generatePrimaryKey("S", "staff_login", "staff_id", out);
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "insert into staff_login values('"+staffId+"', '"+fullName+"', '"+emailId+"', '"+username+"','"+password+"')";
            stmt.executeUpdate(query);
            flag = true;
        } catch(Exception e){
            String printError = "<html>"+
                                "<center><br><br><br><br>"+
                                    "<table>"+
                                        "<tr>"+
                                            "<td> <center> <font size=5 color=red> <b> Invalid Record </b> </font> </center> </td>"+
                                        "</tr>"+
                                        "<tr>"+
                                            "<td>"+e+"</td>"+
                                        "</tr>"+
                                    "</table>"+
                                "</center>"+
                           "</html>";
            out.println(printError);
        } finally{
            closeConnection();
        }
        return flag;
    }
%>