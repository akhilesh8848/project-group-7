<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    String radioButtonStatus = request.getParameter("option");
    String contactNo = request.getParameter("contact_no");
    String customerId = request.getParameter("customerId");
    String planId = request.getParameter("planId");
    
    boolean flag = false;
    if(radioButtonStatus.equals("prepaid")){
        flag = checkSimcardActivateOrNot(true, contactNo, out);
        if(flag){
            addRecordToRechargeTable(customerId, planId, contactNo, radioButtonStatus, out);
            response.sendRedirect("../payment-page.html?customerId="+customerId+"&planId="+planId);
        } else {
            response.sendRedirect("../check-simcard-activated-or-not.html");
        }
    } else {
        flag = checkSimcardActivateOrNot(false, contactNo, out);
        if(flag){
			addRecordToRechargeTable(customerId, planId, contactNo, radioButtonStatus, out);
            response.sendRedirect("../payment-page.html?customerId="+customerId+"&planId="+planId);
        } else {
            response.sendRedirect("../check-simcard-activated-or-not.html");
        }
    }
%>

<%!
    public static boolean checkSimcardActivateOrNot(boolean simType, String contactNo, JspWriter out) throws Exception{
        boolean flag = false;
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "";
            if(simType){
                query = "select * from prepaid where prephno = '"+contactNo+"' and activate_sim_status = 1";
            } else {
                query = "select * from postpaid where postphno = '"+contactNo+"' and activate_sim_status = 1";
            }
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                flag = false;
            } else {
                flag = true;
            }
        } catch(Exception e){
            out.println(" checkSimcardActivateOrNot exception: " +e);
        } finally {
            closeConnection();
        }
        return flag;
    }
%>

<%!
    //Here simcardType = 0 means prepaid and simcardType = 1 means postpaid.
    public static void addRecordToRechargeTable(String customerId, String planId, String contactNo, String simType, JspWriter out) throws Exception {
        int simcardType = 0;
        String postpaidId = "";
        String rechargeId = generatePrimaryKey("R", "recharge", "recharge_no", out);
        List<String> planDetails = fetchPlanDetails(planId, out);
        double planAmount = Double.parseDouble(planDetails.get(2));
        String query = "";
        
        if(simType.equals("prepaid")){
            simcardType = 0;
            query = "insert into recharge values('"+rechargeId+"', '"+planAmount+"', "+simcardType+", '"+planId+"', '"+customerId+"')";
        } else {
            simcardType = 1;
            postpaidId = getPostpaidIdByContactNo(contactNo, out);
            query = "insert into recharge values('"+rechargeId+"', '"+planAmount+"', "+simcardType+", '"+planId+"', '"+customerId+"', '"+postpaidId+"')";
        }
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
        } catch(Exception e){
            out.println(" addRecordToRechargeTable exception: " +e);
        } finally {
            closeConnection();
        }
    }     
%>

<%!    
    public static String getPostpaidIdByContactNo(String contactNo, JspWriter out) throws Exception{
        String postpaidId = "";
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select postpaid_id from postpaid where postphno = '"+contactNo+"'";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                postpaidId = "";
            } else {
                postpaidId = rs.getString("postpaid_id");
            }
        } catch(Exception e){
            out.println(" Get postpaid id by contact no exception: " +e);
        } finally {
            closeConnection();
        }
        return postpaidId;
    }
%>

<%!
    public static List<String> fetchPlanDetails(String planId, JspWriter out) throws Exception{
        List<String> planDetails = new ArrayList<String>();
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select * from plan where plan_no = '"+planId+"'";
            rs = stmt.executeQuery(query);
            rs.next();
            planDetails.add(rs.getString("plan_no"));
            planDetails.add(rs.getString("plan_description"));
            planDetails.add(rs.getString("plan_amount"));
        } catch(Exception e){
            out.println(" fetchPlanDetails exception: " +e);
        } finally {
            closeConnection();
        }
        return planDetails;
    }
%>