<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
	String customerId = request.getParameter("customerId");
	
	try{
		List<List<String>> customerRechargeRecord = fetchCustomerRechargeRecordById(customerId, out);
		if(customerRechargeRecord == null){
			out.println(" Records not found!");
			return;
		}
		String printTable = "";
		int i = 1;
		for(List<String> list : customerRechargeRecord){
			printTable += ""+
				"<tr>"+
					"<td class=\"body-item mbr-fonts-style display-7\">R"+i+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(0)+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(1)+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(2)+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(3)+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(4)+"</td>"+
					"<td class=\"body-item mbr-fonts-style display-7\">"+list.get(5)+"</td>"+
				"</tr>";
				++i;
		}
		out.println(printTable);
	} catch(Exception e){
		out.println(" Show recharge records to customer exception: " +e);
	}
%>

<%!
	public static List<List<String>> fetchCustomerRechargeRecordById(String customerId, JspWriter out) throws Exception{
		List<List<String>> rechargeDetails = new ArrayList<List<String>>();
		List<List<String>> planDetails = getPlanDetailsById(customerId, out);
		try{
			String customerName = fetchCustomerNameById(customerId, out);
			
			con = fetchConnection();
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			String query = "select * from recharge where cust_id = '"+customerId+"'";
			rs = stmt.executeQuery(query);
			if(!rs.next()){
				rechargeDetails = null;
			} else{
				rs.previous();
				int i = 0;
				while(rs.next()){
					List<String> record = new ArrayList<String>();
					record.add(customerName);
					record.add(planDetails.get(i).get(0));
					record.add(planDetails.get(i).get(1));
					record.add(planDetails.get(i).get(2));
					record.add(planDetails.get(i).get(3));
					record.add(rs.getString("recharge_amount"));
					rechargeDetails.add(record);
					++i;
				}
			}
		} catch(Exception e){
			out.println(" Fetch customer recharge record by id exception: " +e);
		} finally {
			closeConnection();
		}
		return rechargeDetails;
	}
%>

<%!
	public static String fetchCustomerNameById(String customerId, JspWriter out) throws Exception{
		String customerName = "";
		try{
			con = fetchConnection();
			stmt = con.createStatement();
			String query = "select * from customer_login where cust_id = '"+customerId+"'";
			rs = stmt.executeQuery(query);
			if(!rs.next()){
				customerName = null;
			} else {
				customerName = rs.getString("name");
			}
		} catch(Exception e){
			out.println(" Fetch customer name by id exception: " +e);
		} finally{
			closeConnection();
		}
		return customerName;
	}
%>

<%!
	public static List<List<String>> getPlanDetailsById(String customerId, JspWriter out) throws Exception {
		List<List<String>> planDetails = new ArrayList<List<String>>();
		try{
			con = fetchConnection();
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			String query = "select plan_description from plan where plan_no in (select plan_no from recharge where cust_id = '"+customerId+"')";
			rs = stmt.executeQuery(query);
			if(!rs.next()){
				planDetails = null;
			} else {
				rs.previous();
				while(rs.next()){
					List<String> record = new ArrayList<String>();
					StringTokenizer split = new StringTokenizer(rs.getString("plan_description"), "$");
					
					record.add(split.nextToken() + " GB");
					split.nextToken();
					record.add(split.nextToken() + " MIN");
					split.nextToken();
					record.add(split.nextToken() + " SMS");
					split.nextToken();
					record.add(split.nextToken() + " DAYS");
					split.nextToken();
					
					planDetails.add(record);
				}
			}
		} catch(Exception e){
			out.println(" Plan details by id exception: " +e);
		} finally {
			closeConnection();
		}
		return planDetails;
	}
%>