<%@ page language="java" import="java.sql.*, java.io.*, java.util.*, java.time.*, com.oreilly.servlet.MultipartRequest"%>
<%@include file="database-operations.jsp" %>

<%
	String customerId = request.getParameter("customerId");
	out.println(customerId);
    String fileName = request.getParameter("fileName");
    String prepaidPostpaidFlag = request.getParameter("pre_post_flag");
    String folderName = "upload-documents";
    
    String id = "";
    String tableName = "";
    int prepostFlag = 0;
    if(prepaidPostpaidFlag.equals("0")){
        id = customerId;
        prepostFlag = 0;
        tableName = "prepaid";
    } else {
        id = customerId;
        prepostFlag = 1;
        tableName = "postpaid";
    }

    try{
        String path = System.getProperty("user.dir");
        String uploadFolderPath = path + File.separator +".."+File.separator + "webapps" + File.separator + "E-Airtel-Project" + File.separator;
        boolean isExists = isDirectoryExists(path, folderName, out);
        if(!isExists){
            createDirectory(uploadFolderPath, folderName, out);
        }
        MultipartRequest m = new MultipartRequest(request, uploadFolderPath + folderName);
        changeFileName(fileName,  uploadFolderPath + folderName, tableName, id, prepostFlag, out);
        out.print("Photo uploaded successfully.");  
    } catch(Exception e){
        out.println(" Exception: " +e);
    }
%>

<%!
    public static String getLatestId(String tableName, String primaryKey, JspWriter out) throws Exception{
        String id = "";
        try{
            con = fetchConnection();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String query = "select "+primaryKey+" from "+tableName+"";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                id = null;
            } else {
                id = rs.getString(primaryKey);
            }
        } catch(Exception e){
            out.println(" Get latest id exception: " +e);
        } finally {
            closeConnection();
        }
        return id;
    }
%>

<%!
    public static boolean isDirectoryExists(String path, String folderName, JspWriter out) throws Exception{
        boolean flag = false;
        try{
            File file = new File(path + folderName);
            if(file.exists()){
                flag = true;
            }
        } catch(Exception e){
            out.println(" isDirectoryExists: " +e);
        }
        return flag;
    }
%>

<%!
    public static void changeFileName(String fileName, String path, String tableName, String primaryKey, int prepostFlag, JspWriter out) throws Exception{
        try{
            StringTokenizer splitString = new StringTokenizer(fileName, ".");
            splitString.nextToken(); // Its filename not an extension thats why we traverse it.
            String extension = splitString.nextToken();
            LocalDate currentDate = LocalDate.now();
            String newFileName = generateNewFileName() + "." + extension;
            File originalFile = new File(path + File.separator + fileName);
            File renameFile = new File(path + File.separator + newFileName);
            originalFile.renameTo(renameFile);
            updateDocumentProof(path + File.separator + newFileName, tableName, prepostFlag, primaryKey, out);
        } catch(Exception e){
            out.println(" isDirectoryExists: " +e);
        }
    }
%>

<%!
    public static void updateDocumentProof(String path, String tableName, int prepostFlag, String primaryKey, JspWriter out) throws Exception{
        try{
            String query = "";
            if(prepostFlag == 0){
                query = "update "+tableName+" set document_proof = '"+path+"' where prepaid_id = '"+primaryKey+"'";
            } else {
                query = "update "+tableName+" set document_proof = '"+path+"' where postpaid_id = '"+primaryKey+"'";
            }
            con = fetchConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
        } catch(Exception e){
            out.println(" Update document proof exception: " +e);
        } finally {
            closeConnection();            
        }
    }
%>

<%!
    public static void createDirectory(String path, String folderName, JspWriter out) throws Exception {
        try{
            new File(path + folderName).mkdir();
        } catch(Exception e){
            out.println(" createDirectory: " +e);
        }
    }
%>

<%!
	public static String generateNewFileName(){
		LocalDate currentDate = LocalDate.now();
		LocalTime currentTime = LocalTime.now();
		
		return "AADHAR-" + currentDate.getYear() + "" + currentDate.getMonthValue() + "" + currentDate.getDayOfMonth() + "" +
							currentTime.getHour() + "" + currentTime.getMinute() + "" + currentTime.getSecond();
	}
%>
