<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
	String customerId = request.getParameter("customerId");
    String firstName = request.getParameter("first_name");
    String middleName = request.getParameter("middle_name");
    String lastName = request.getParameter("last_name");
    
    String address = request.getParameter("address");
    String city = request.getParameter("city");
    String state = request.getParameter("state");

    String birthDate = request.getParameter("date_of_birth");
    String issueDate = request.getParameter("issue_date");
    String contactNo = request.getParameter("contact_no");
    
    String simSerialNo = request.getParameter("sim_no");
    String gender = request.getParameter("gender");
    String prepaid = request.getParameter("prepaid");
    String postpaid = request.getParameter("postpaid");
    String documentProff = "";
    int activateStatus = 0;
    
    boolean flag = false;
    if(prepaid.equalsIgnoreCase("yes")){
        flag = storeCustomerDetails(out, firstName, middleName, lastName, address, city, state, birthDate, issueDate, contactNo, simSerialNo, gender, documentProff, activateStatus, true, customerId);
        if(flag){
            response.sendRedirect("../prepaid-add-document.html?customerId="+customerId);
        } else {
            response.sendRedirect("../error-page.html");
        }
    } else if(postpaid.equalsIgnoreCase("yes")){
        flag = storeCustomerDetails(out, firstName, middleName, lastName, address, city, state, birthDate, issueDate, contactNo, simSerialNo, gender, documentProff, activateStatus, false, customerId);
        if(flag){
            response.sendRedirect("../postpaid-add-document.html?customerId="+customerId);
        } else {
            response.sendRedirect("../error-page.html");
        }
    } else {
        out.println(" Not selected prepaid / postpaid");
    }
%>

<%!
    public static boolean storeCustomerDetails(JspWriter out, String firstName, String middleName, String lastName, String address, String city, String state, String birthDate, String issueDate, String contactNo, String simSerialNo, String gender, String documentProff, int activateStatus, boolean flag, String customerId) throws Exception{
        boolean transactionFlag = false;
        try{
            String query = "";
            String primaryKey = "";
            if(flag){
				query = "update prepaid set prefname = '"+firstName+"', premname = '"+middleName+"', prelname = '"+lastName+"', preadd = '"+address+"', precity = '"+city+"', prestate = '"+city+"', prebirthdate = '"+birthDate+"', prephno = '"+contactNo+"', presimno = '"+simSerialNo+"', preissue = '"+issueDate+"', pregender = '"+gender+"', document_proof = '"+documentProff+"', activate_sim_status = '"+activateStatus+"' where prepaid_id = '"+customerId+"'";
            } else {
				query = "update postpaid set postfname = '"+firstName+"', postmname = '"+middleName+"', postlname = '"+lastName+"', postadd = '"+address+"', postcity = '"+city+"', poststate = '"+city+"', postbirthdate = '"+birthDate+"', postphno = '"+contactNo+"', postsimno = '"+simSerialNo+"', postissue = '"+issueDate+"', postgender = '"+gender+"', document_proof = '"+documentProff+"', activate_sim_status = '"+activateStatus+"' where postpaid_id = '"+customerId+"'";
            }
            
            con = fetchConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            transactionFlag = true;
        } catch(Exception e){
            out.println(" Store customer exception: " +e);
        } finally{
            closeConnection();
        }
        return transactionFlag;
    }
%>