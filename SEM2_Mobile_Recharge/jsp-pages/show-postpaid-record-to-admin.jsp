<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    try{
        List<List<String>> postpaidDetails = getPostpaidDetails(out);
        if(postpaidDetails == null){
            out.println("No records found!");
        } else {
            String printDetails = "";
            int i = 1;
            for(List<String> list : postpaidDetails){
                List<String> planDetails = getPlanDetailsById(list.get(0), out);
                if(planDetails != null){
                    String URL = "generate-bill.html?customerId="+planDetails.get(4)+"&planNo="+planDetails.get(3)+"&rechargeId="+planDetails.get(0);
                    printDetails += ""+
                        "<tr>"+
                        "<td class=\"body-item mbr-fonts-style display-7\">"+i+
                        "</td>"+
                        "<td class=\"body-item mbr-fonts-style display-7\">"+list.get(1)+
                        "</td>"+
                        "<td class=\"body-item mbr-fonts-style display-7\">"+list.get(2)+
                        "</td>"+
                        "<td class=\"body-item mbr-fonts-style display-7\">"+
                            "<a href="+URL+">"+
                                "<font color=\"red\">GENERATE BILL</font>"+
                            "</a>"+
                        "</td>"+
                    "</tr>";
                    ++i;
                }
            }
            out.println(printDetails);
        }
    } catch(Exception e){
        out.println(" Show postpaid record exception: " +e);
    }
%>

<%!
    public static List<String> getPlanDetailsById(String postpaidId, JspWriter out) throws Exception{
        List<String> planDetails = new ArrayList<String>();
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select * from recharge where postpaid_id = '"+postpaidId+"'";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                planDetails = null;
            } else {
                planDetails.add(rs.getString("recharge_no"));
                planDetails.add(rs.getString("recharge_amount"));
                planDetails.add(rs.getString("prepaid_postpaid_flag"));
                planDetails.add(rs.getString("plan_no"));
                planDetails.add(rs.getString("cust_id"));
                planDetails.add(rs.getString("postpaid_id"));
            }
        } catch(Exception e){
            out.println(" Get plan details by id exception: " +e);
        } finally {
            closeConnection();
        }
        return planDetails;
    }
%>

<%!
    public static List<List<String>> getPostpaidDetails(JspWriter out) throws Exception{
        List<List<String>> postpaidDetails = new ArrayList<List<String>>();
        try{
            con = fetchConnection();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String query = "select * from postpaid";
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                postpaidDetails = null;
            } else {
                rs.previous();
                while(rs.next()){
                    List<String> record = new ArrayList<String>();
                    record.add(rs.getString("postpaid_id"));
                    record.add(rs.getString("postfname") + " " + rs.getString("postmname") + " " + rs.getString("postlname"));
                    record.add(rs.getString("postphno"));
                    postpaidDetails.add(record);
                }
            }
        } catch(Exception e){
            out.println(" Get postpaid details exception: " +e);
        } finally {
            closeConnection();
        }
        return postpaidDetails;
    }
%>