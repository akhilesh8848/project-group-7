<%@ page language="java" %>
<%@include file="common-database.jsp" %>

<%
    String radioButtonStatus = request.getParameter("option");
    String contactNo = request.getParameter("contact_no");
    if(radioButtonStatus.equals("prepaid")){
        checkContactPresentOrNot(response, out, contactNo, true);
    } else {
        checkContactPresentOrNot(response, out, contactNo, false);
    }
%>

<%!
    public static boolean checkContactPresentOrNot(HttpServletResponse res, JspWriter out, String contactNo, boolean flag) throws Exception{
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "";
            if(flag){
                query = "select prephno from prepaid where prephno = '"+contactNo+"'";
            } else {
                query = "select postphno from postpaid where postphno =  '"+contactNo+"'";
            }
            rs = stmt.executeQuery(query);
            if(!rs.next()){
                out.println(" There is no record present in database.");
            } else {
                if(flag){
                    res.sendRedirect("../edit-prepaid-customer.jsp?contactNo="+contactNo);
                } else {
                    res.sendRedirect("../edit-postpaid-customer.jsp?contactNo="+contactNo);
                }
            }
        } catch(Exception e){
            out.println(" Check contact exception: " +e);
        } finally{
            closeConnection();
        }
        return true;
    }
%>