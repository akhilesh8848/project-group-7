<%@page language="java" import="java.sql.*"%>
<%@include file="database-operations.jsp" %>

<%
    String customerId = request.getParameter("customerId");
    String planId = request.getParameter("planId");
    try{
        float amount = getAmountById(planId, out);
        out.println(amount);
    } catch(Exception e){
        out.println(" Get amount exception: " +e);
    }
%>

<%!
    public static float getAmountById(String planId, JspWriter out) throws Exception {
        float amount = 0.0f;
        try{
            con = fetchConnection();
            stmt = con.createStatement();
            String query = "select plan_amount from plan where plan_no = '"+planId+"'";
            rs = stmt.executeQuery(query);
            rs.next();
            amount = rs.getFloat("plan_amount");
        } catch(Exception e){
            out.println(" Get amount by id exception: " +e);
        } finally {
            closeConnection();
        }
        return amount;
    }
%>