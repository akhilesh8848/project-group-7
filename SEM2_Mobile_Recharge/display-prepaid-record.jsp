<!--
    
    Project Name: E-Airtel
-->

<%@page language="java" import="java.sql.*"%>
<%@include file="jsp-pages/common-database.jsp" %>


<!DOCTYPE html>
<html >
    <head>
        <!-- Site made with Mobirise Website Builder v4.7.6, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v4.7.6, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/airtel-logo-red-text-horizontal-122x50.jpg" type="image/x-icon">
        <meta name="description" content="Web Generator Description">
        <title>Prepaid | Record</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/soundcloud-plugin/style.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    </head>
    <body>
        <section class="menu cid-qTCFVt5HNb" once="menu" id="menu1-f">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                        <a href="">
                        <img src="assets/images/airtel-logo-red-text-horizontal-122x50.jpg" alt="Mobirise" title="" style="height: 3.8rem;">
                        </a>
                        </span>
                        <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="">E-Airtel</a></span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="index.html"><span class="mbri-unlock mbr-iconfont mbr-iconfont-btn"></span>
                            Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="engine"><a href="https://mobirise.me/p">site templates free download</a></section>
        <section class="section-table cid-qTCGL1sYKA" id="table1-g">
            <div class="container container-table">
                <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2">
                    Table
                </h2>
                <div class="table-wrapper">
                    <div class="container">
                    </div>
                    <div class="container scroll">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr class="table-heads ">
                                    <th class="head-item mbr-fonts-style display-7">
                                        Name
                                    </th>
                                    
                                    <th class="head-item mbr-fonts-style display-7">
                                        City
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        State
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        Birthdate
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        Issue Date
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        Contact No.
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        Sim Serial No
                                    </th>

                                    <th class="head-item mbr-fonts-style display-7">
                                        Gender
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                    <%
                                        try {
                                            con = fetchConnection();
                                            stmt = con.createStatement();
                                            String query = "select * from prepaid";
                                            rs = stmt.executeQuery(query);
                                            while(rs.next()){
                                                %><tr><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("prefname")+" "+rs.getString("premname")+" "+rs.getString("prelname")%></td><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("precity")%></td><%

                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("prestate")%></td><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("prebirthdate")%></td><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("preissue")%></td><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("prephno")%></td><%

                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("presimno")%></td><%
                                                    %><td class="body-item mbr-fonts-style display-7"><%=rs.getString("pregender")%></td><%
                                                %></tr><%
                                            }
                                        } catch(Exception e) {
                                            out.println(" Prepaid record exception: " +e);
                                        } finally {
                                            closeConnection();
                                        }
                                    %>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="container table-info-container">
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/dropdown/js/script.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/datatables/jquery.data-tables.min.js"></script>
        <script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/theme/js/script.js"></script>
    </body>
</html>