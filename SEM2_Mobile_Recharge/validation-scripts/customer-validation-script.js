/*
    Name validation:
    Name must not contains any number.
*/
function nameValidation(name){
    var letters = /^[A-Za-z]+$/;
    if(!letters.test(name)){
        return true;
    }
    return false;
}

function firstNameValidate(){
    var firstName = document.getElementById("first_name").value;
    if(nameValidation(firstName)){
        alert("Name does not contains numbers.");
        document.getElementById("first_name").value = "";        
    }
}

function middleNameValidate(){
    var middleName = document.getElementById("middle_name").value;
    if(nameValidation(middleName)){
        alert("Name does not contains numbers.");
        document.getElementById("middle_name").value = "";        
    }
}

function lastNameValidate(){
    var lastName = document.getElementById("last_name").value;
    if(nameValidation(lastName)){
        alert("Name does not contains numbers.");
        document.getElementById("last_name").value = "";
    }
}

function addressValidation(){
    var address = document.getElementById("address").value;
    if(address.length < 20){
        alert(" Addresss must contains 20 characters");
        document.getElementById("address").value = "";
    }
}

function cityValidation(){
    var city = document.getElementById("city").value;
    if(nameValidation(city)){
        alert("City does not contains numbers.");
        document.getElementById("city").value = "";
    }
}

function stateValidation(){
    var state = document.getElementById("state").value;
    if(nameValidation(state)){
        alert("State does not contains numbers.");
        document.getElementById("state").value = "";
    }    
}

function contactNoValidation(){
    var contactNo = document.getElementById("contact_no").value;
    var letters = /^[0-9]+$/;
    if(!letters.test(contactNo)){
        alert("Contact No does not contains alphanumeric letters.");
        document.getElementById("contact_no").value = "";
    }
}

function genderValidation(){
    var gender = document.getElementById("gender").value;
    var genderStr1 = /^[Male]+$/;
    var genderStr2 = /^[male]+$/;
    var genderStr3 = /^[Female]+$/;
    var genderStr4 = /^[female]+$/;

    if(genderStr1.test(gender) || genderStr2.test(gender) || genderStr3.test(gender) || genderStr4.test(gender)){
    } else {
        alert(" Invalid gender");
        document.getElementById("gender").value = "";
    }
}

function dateValidation(date){
    var dateStr = /^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]+$/;
    var msg = "";
   
    if(dateStr.test(date)){
       var splitDate = date.split("-");
       if(splitDate[1] >= 1 && splitDate[1] <= 12){
           if(splitDate[2] >= 1 && splitDate[2] <= 31){
           } else {
               msg = "Invalid Day";
               alert(msg);
               return false;
           }
       } else {
            msg = "Invalid Month"
            alert(msg);
            return false;
       }
    } else {
        alert(" Date must be in (yyyy/mm/dd) format. \n For E.g. 2019-02-20");
        return false;
    }
    return true;
}

function issueDateValidate(){
    var date = document.getElementById("issue_date").value;
    if(!dateValidation(date)){
        document.getElementById("issue_date").value = "";
    }
}

function birthDateValidation(){
    var date = document.getElementById("date_of_birth").value;
    if(!dateValidation(date)){
        document.getElementById("date_of_birth").value = "";
    }
}

function serialNumberValidation(){
    var serialNo = document.getElementById("sim_no").value;
    var numberStr = /^[0-9]+$/;
    if(numberStr.test(serialNo)){
        if(serialNo.length != 15){
            alert("Serial number must be the 15 digits.");
            document.getElementById("sim_no").value = "";
        }
    } else {
        alert("INVALID");
        document.getElementById("sim_no").value = "";
    }
}