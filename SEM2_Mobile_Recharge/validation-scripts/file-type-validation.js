function selectedFile(originalFile, customerId) {
	if(checkTypeOfFile(originalFile.value)){
        uploadAadharCardForPrepaidCustomer(customerId);
	} else {
		alert("Invalid file");
	}
}
     
function checkTypeOfFile(fileName) {
	var extension = fileName.split(".")[1];
	var flag = false;
	if(extension == "jpg" || extension == "png" || extension == "jpeg"){
		flag = true;
	}
	return flag;
}

function uploadAadharCardForPrepaidCustomer(customerId){
    xmlhttp = new XMLHttpRequest();
    var imageFile = document.documentForm.myAadharCard.files[0];
    var fileName = imageFile.name;

    var formData = new FormData();
    formData.append("file", imageFile);
    xmlhttp.open("POST","/E-Airtel-Project/jsp-pages/upload-addhar-card.jsp?fileName="+fileName+"&pre_post_flag=0&customerId="+customerId,false);
    xmlhttp.send(formData);
    output = xmlhttp.responseText;
    document.getElementById("aadharCardStatus").innerHTML = ""+output;
}